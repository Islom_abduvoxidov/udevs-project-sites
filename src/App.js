import { Switch, Route } from "react-router";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import "./index.css";

import Home from "./Pages/Home/Home";
import Development from "./Pages/Development/Development";
import Admins from "./Pages/Admins/Admins";
import Design from "./Pages/Design/Design";
import Menejment from "./Pages/Menejment/Menejment";
import Marketing from "./Pages/Marketing/Marketing";
import Nauchpop from "./Pages/Nauchpop/Nauchpop";
import Read from "./Pages/Home/Read";
import Avtor from "./components/Avtor/Avtor";

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/development" exact>
          <Development />
        </Route>
        <Route path="/admins" exact>
          <Admins />
        </Route>
        <Route path="/design" exact>
          <Design />
        </Route>
        <Route path="/menejment" exact>
          <Menejment />
        </Route>
        <Route path="/marketing" exact>
          <Marketing />
        </Route>
        <Route path="/nauchpop" exact>
          <Nauchpop />
        </Route>
        <Route path="/home/:id" exact>
          <Read />
        </Route>
        {window.localStorage.getItem('userToken') && <Route path="/avtor" exact>
          <Avtor />
        </Route>}
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
