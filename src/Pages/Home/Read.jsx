import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import img from './img/home-1.png';
import axios from 'axios';

function Read() {
  const read = useParams();
  const [post, setPost] = useState(null);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${read.id}`)
      .then((res) => {
        setPost(res.data);
      })
      .catch((error) => {
        console.log('error:', error);
      });
  }, []);

  useEffect(() => {
    axios
      .get('https://reqres.in/api/users?page=1')
      .then((res) => {
        setUsers(res.data.data);
      })
      .catch((error) => {
        console.log('error:', error);
      });
  }, []);

  return (
    <div className="container read__content">
      <div className="read">
        <img src={img} alt="" className="read__img" />
        <div className="read__write">
          <h2 className="read__title">{post?.title}</h2>
          <p className="read__text">
            {post?.body}
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia
              accusantium exercitationem ea nemo placeat! Libero deleniti
              repellendus autem quis tenetur?
            </span>
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia
              accusantium exercitationem ea nemo placeat! Libero deleniti
              repellendus autem quis tenetur?
            </span>
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia
              accusantium exercitationem ea nemo placeat! Libero deleniti
              repellendus autem quis tenetur Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Dolores beatae cumque quo. Dolorum
              aliquam at voluptatem, sed reiciendis ex necessitatibus aut quos
              doloremque harum expedita culpa nesciunt rem accusantium
              asperiores.
            </span>
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia
              accusantium exercitationem ea nemo placeat! Libero deleniti
              repellendus autem quis tenetur?
            </span>
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia
              accusantium exercitationem ea nemo placeat! Libero deleniti
              repellendus autem quis tenetur Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Dolores beatae cumque quo. Dolorum
              aliquam at voluptatem, sed reiciendis ex necessitatibus aut quos
              doloremque harum expedita culpa nesciunt rem accusantium
              asperiores.
            </span>
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia
              accusantium exercitationem ea nemo placeat! Libero deleniti
              repellendus autem quis tenetur? Lorem ipsum dolor sit, amet
              consectetur adipisicing elit. Soluta non qui quod minima incidunt
              quos, nostrum expedita recusandae sunt earum?
            </span>
          </p>
          <div className="avtor">
            <Link className="avtor__link" to={window.localStorage.getItem('userToken') ? "/avtor" : "/"}>
              <img src={img} alt="img" className="avtor__img" />
            </Link>
            <Link className="avtor__link" to={window.localStorage.getItem('userToken') ? "/avtor" : "/"}>
              <p className="avtor__name">Автор: Dilorom Alieva</p>
            </Link>
          </div>
        </div>
      </div>
      <aside className="read__aside">
        <h2 className="aside__title">ЛУЧШИЕ БЛОГИ</h2>
        {users.map((user) => {
          return (
            <div key={user.id} className="aside__box">
              <img src={user.avatar} alt="img" />
              <div className="aside__write">
                <h3 className="aside__subtitle">
                  <span className="title__name">{user.first_name}</span>&nbsp;
                  <span className="title__surname">{user.first_name}</span>
                </h3>
                <p className="aside__text">
                  Lorem ipsum dolor sit amet consectetur.
                </p>
                <p className="aside__time">{user.email}</p>
              </div>
            </div>
          );
        })}
      </aside>
    </div>
  );
}

export default Read;
