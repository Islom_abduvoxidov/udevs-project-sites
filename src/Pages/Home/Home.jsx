import React, { useRef } from 'react';
import './Home.css';
import HomeImg from './img/home-1.png';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';

function Home() {
  const [posts, setPosts] = useState([]);
  const ref = useRef();

  useEffect(() => {
    axios
      .get('https://jsonplaceholder.typicode.com/posts')
      .then((res) => {
        setPosts(res.data.splice(0, 5));
      })
      .catch((error) => {
        console.log('error:', error);
      });
  }, []);

  function onGridHandle(e) {
    ref.current.classList.add('active');
  }
  function removeGridHandle(e) {
    ref.current.classList.remove('active');
  }

  return (
    <div className="container home">
      <div className="home__filter">
        <h2 className="filter__title">News</h2>
        <div className="filter__btns">
          <button onClick={onGridHandle} className="filter__btn">
            <i className="bx bxs-grid-alt"></i>
          </button>
          <button onClick={removeGridHandle} className="filter__btn">
            <i className="bx bx-columns bx-rotate-270"></i>
          </button>
        </div>
      </div>
      <div ref={ref} className="home__list">
        {posts.map((post) => (
          <li key={post.id} className="list__item">
            <Link className="link" to={`/home/${post.id}`}>
              <img className="home__imge" src={HomeImg} alt="home img" />
            </Link>
            <div className="home__write">
              <p className="home__date">
                {new Date(Date.now()).toDateString()}
              </p>
              <Link className="link" to={`/home/${post.id}`}>
                <h3 className="home__title">{post.title}</h3>
              </Link>
              <p className="home__text">
                {post.body} Lorem ipsum dolor sit amet consectetur adipisicing
                elit. Fuga, excepturi?
              </p>
              <Link className="home__link" to={`/home/${post.id}`}>
                Читать
              </Link>
            </div>
          </li>
        ))}
      </div>
    </div>
  );
}

export default Home;
