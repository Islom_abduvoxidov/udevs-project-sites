import React, { useRef, useState } from "react";
import "./Header.css";
import Logo from "./logo.svg";
import { Link, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure()
function Header() {
  const ref__list = useRef();
  const ref__modal = useRef();
  const ref__email = useRef();
  const ref__password = useRef();
  const [popup, setPopup] = useState(false);
  const history = useHistory();

  const notify = () => {
    toast.error("Login yoki parol xato!", {
      position: "top-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  }

  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    onSubmit: (values) => {

      if (
        user.email === values.email &&
        user.password === values.password
      ) {
        window.localStorage.setItem("userToken", true);
        JSON.parse(window.localStorage.getItem("userToken"));
        history.push("/avtor");
        setPopup(false);
        window.location.reload();
      } else {
        notify();
      }
    }
  })

  const getToken = JSON.parse(window.localStorage.getItem('userToken'));

  const user = {
    email: "islom@gmail.com",
    password: "islom"
  }

  function showMenu(e) {
    const target = e.target;
    console.log(target);
    if (target) {
      if (!ref__list.current.classList.contains("show")) {
        ref__list.current.classList.add("show");
        document.body.classList.add("hidden");
      } else {
        ref__list.current.classList.remove("show");
        document.body.classList.remove("hidden");
      }
    }
  }

  function modalHendle(e) {
    setPopup(true);
  }
  function modalHideHendle(e) {
    if (e.target.className === "modal") {
      setPopup(false);
    }
  }

  return (
    <header>
      <div className="container header__content">
        <div className="header__logo">
          <div onClick={showMenu} className="burger__menu">
            <span className="menu__block"></span>
          </div>
          <Link to="/">
            <img src={Logo} alt="imge logo" width="146" height="49" />
          </Link>
        </div>
        <ul ref={ref__list} className="header__list">
          <li className="header__item">
            <Link className="header__link" to="/">
              Все потоки
            </Link>
          </li>
          <li className="header__item">
            <Link className="header__link" to="/development">
              Разработка
            </Link>
          </li>
          <li className="header__item">
            <Link className="header__link" to="/admins">
              Администрирование
            </Link>
          </li>
          <li className="header__item">
            <Link className="header__link" to="/design">
              Дизайн
            </Link>
          </li>
          <li className="header__item">
            <Link className="header__link" to="/menejment">
              Менеджмент
            </Link>
          </li>
          <li className="header__item">
            <Link className="header__link" to="/marketing">
              Маркетинг
            </Link>
          </li>
          <li className="header__item">
            <Link className="header__link" to="/nauchpop">
              Научпоп
            </Link>
          </li>
        </ul>

        <div className="header__login">
          {getToken ? (
            <Link className="user__token--link" to="/avtor">
              <i class="bx bxs-user-circle"></i>
            </Link>
          ) : (
            <button onClick={modalHendle} className="login__btn" type="button">
              Войти
            </button>
          )}
        </div>
      </div>
      {popup && (
        <div ref={ref__modal} className="modal__show">
          <div onClick={modalHideHendle} className="modal">
            <div className="modal__content">
              <h2 className="modal__title">Вход на udevs news</h2>

              <form onSubmit={formik.handleSubmit} className="modal__form">
                <input
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  ref={ref__email}
                  placeholder="Email"
                  name="email"
                  required
                  type="email"
                  className="modal__input"
                />
                <input
                  ref={ref__password}
                  onChange={formik.handleChange}
                  placeholder="Пароль"
                  required
                  name="password"
                  type="password"
                  className="modal__input"
                />
                <button className="modal__btn" type="submit">
                  Войти
                </button>
              </form>
            </div>
          </div>
        </div>
      )}
    </header>
  );
}

export default Header;
