import React from "react";
import { useHistory } from "react-router-dom";
import "./Avtor.css"
import imgAvtor from "../../Pages/Home/img/home-1.png"
import Home from "../../Pages/Home/Home"

function Avtor() {
  const history = useHistory();
function logOutHandle() {
  window.localStorage.removeItem("userToken");
  history.push("/")
  window.location.reload()
}
  

  return (
    <>
      <div className="container avtor__profile">
        <div className="avtor__info">
          <img className="avotr__imge" src={imgAvtor} alt="" />
          <div className="avtor__write">
            <h2 className="avtor__title">Дилором Алиева</h2>
            <p className="avtor__text">
              Карьера
              <span className="avtor__subtext">Писатель</span>
            </p>
            <p className="avtor__text">
              Дата рождения
              <span className="avtor__subtext">Дата рождения</span>
            </p>
            <p className="avtor__text">
              Место рождения
              <span className="avtor__subtext">Писатель</span>
            </p>
          </div>
        </div>
        <div className="log__out">
          <button
          onClick={logOutHandle}
          type="button">Logout</button>
        </div>
        <div className="populer__posts">
          <h2 className="popular__title">ПУБЛИКАЦИИ</h2>
        </div>
      </div>
      <Home/>
    </>
  );
}

export default Avtor;
