import React from 'react'
import "./Footer.css"
import Logo from "../Header/logo.svg";

function Footer() {
   return (
      <footer>
         <div className="container footer__content">
            <div className="info__footer">
               <div className="footer__intro">
                  <img src={Logo} alt="footer logo" className="footer__logo" width="103" height="35" />
                  <p className="footer__text">Помощник в публикации статей, журналов.
                  Список популярных международных конференций.
                  Всё для студентов и преподавателей.</p>
               </div>
               <ul className="footer__list">
                  <li className="footer__item">
                     <h3 className="footer__title">Ресурсы</h3>
                  </li>
                  <li className="footer__item">Статьи</li>
                  <li className="footer__item">Журналы</li>
                  <li className="footer__item">Газеты</li>
                  <li className="footer__item">Диплом</li>
               </ul>
               <ul className="footer__list">
                  <li className="footer__item">
                     <h3 className="footer__title">О нас</h3>
                  </li>
                  <li className="footer__item">Контакты</li>
                  <li className="footer__item">Контакты</li>
                  <li className="footer__item">Заявки</li>
                  <li className="footer__item">Политика</li>
               </ul>
               <ul className="footer__list">
                  <li className="footer__item">
                     <h3 className="footer__title">Помощь</h3>
                  </li>
                  <li className="footer__item">Часто задаваемые <br /> вопросы</li>
               </ul>
            </div>
            <p className="footer__bottom">Copyright © 2020. LogoIpsum. All rights reserved.</p>
         </div>
      </footer>
      )
   }
   
   export default Footer;
   